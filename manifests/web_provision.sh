#! /bin/bash

echo -e "--- wasce-server on Ubuntu 10.04 LTS ---"
echo -e "--- updating system- --"

cat << EOF | sudo tee -a /etc/motd.tail
*****************************************************************************************
__  _  _______    ______ ____  ____             ______ ______________  __ ___________ 
\ \/ \/ /\__  \  /  ___// ___\/ __ \   ______  /  ___// __ \_  __ \  \/ // __ \_  __ \
 \     /  / __ \_\___ \\  \__\  ___/  /_____/  \___ \\  ___/|  | \/\   /\  ___/|  | \/
  \/\_/  (____  /____  >\___  >___  >         /____  >\___  >__|    \_/  \___  >__|   
              \/     \/     \/    \/               \/     \/                 \/       

					Welcome to Ubuntu 10.04 Lucid WAS 8.5.5 server
             					Vagrant Box

*****************************************************************************************
EOF

##### VARIABLES #####

# Throughout this script, some variables are used, these are defined first.
# These variables can be altered to fit your specific needs or preferences.

# Server name
HOSTNAME="vagrant.dcl"

# Locale
LOCALE_LANGUAGE="en_US" # can be altered to your prefered locale, see http://docs.moodle.org/dev/Table_of_locales
LOCALE_CODESET="en_US.UTF-8"

# Timezone
TIMEZONE="Europe/Paris" # can be altered to your specific timezone, see http://manpages.ubuntu.com/manpages/jaunty/man3/DateTime::TimeZone::Catalog.3pm.html

VM_ID_ADDRESS="192.168.66.6"

#----- end of configurable variables -----#

# prepend "mirror" entries to sources.list to let apt-get use the most performant mirror
sudo sed -i -e '1ideb mirror://mirrors.ubuntu.com/mirrors.txt precise main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt precise-updates main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt precise-backports main restricted universe multiverse\ndeb mirror://mirrors.ubuntu.com/mirrors.txt precise-security main restricted universe multiverse\n' /etc/apt/sources.list
sudo apt-get update

echo "[vagrant provisioning] Installing Java..."
sudo apt-get -y install curl
sudo apt-get -y install python-software-properties # adds add-apt-repository

sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update

# automatic install of the Oracle JDK 7
echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections

sudo apt-get -y install oracle-java7-set-default

export JAVA_HOME="/usr/lib/jvm/java-7-oracle/jre"

echo "[vagrant provisioning] Installing WASCE-server..."

echo 'deb http://archive.canonical.com/ubuntu lucid partner' >/tmp/myppa.list
echo 'deb-src http://archive.canonical.com/ubuntu lucid partner' >>/tmp/myppa.list
cp /tmp/myppa.list /etc/apt/sources.list.d/
rm /tmp/myppa.list

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update --fix-missing
echo -e "\n--- Install packages ---\n"
apt-get install -y -qq build-essential \
                   vim \
                   tree \
                   mc \
                   tmux \
                   git-core \
                   cvs \
                   wasce-server
echo -e "\n--- Upgrade packages ---\n"
apt-get upgrade -y -qq
apt-get clean -y -qq

